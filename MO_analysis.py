import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

sum = lambda x,y: x+y
def extract_race(xs, ys, race_arr, race):
    return xs[race_arr == race], ys[race_arr == race]

def make_scatter_plot(xs, ys, s=4.**(-6)):
    plt.scatter(xs, ys, s=s)
    plt.axis('off')

def gen_hist_2d(xs, ys, nbins=1024):
    H, xedges, yedges = np.histogram2d(ys, xs, bins=nbins)
    H = np.rot90(H)
    H = np.flipud(H)
    return H

def make_hist_sorted(xs, ys, nbins2D, size=1.0, color='black', label='', inv=False):
    H_sorted = np.trim_zeros(np.sort(gen_hist_2d(xs, ys, nbins=nbins2D).flatten()))
    H_sorted = H_sorted*(nbins2D**2)/len(xs)
    if(inv): H_sorted = H_sorted[::-1]
    x = np.linspace(1./len(H_sorted), 1.0, len(H_sorted))
    #plt.scatter(x, H_sorted, s=size, color=color, label=label)
    plt.scatter(H_sorted, x, s=size, color=color, label=label)

def make_comp_cumulative_dist(xs, ys, nbins2D, nbins1D=128, size=1.0, color='black', label=''):
    H_sorted = np.trim_zeros(np.sort(gen_hist_2d(xs, ys, nbins=nbins2D).flatten()))
    H_sorted = H_sorted*(nbins2D**2)/len(xs)
    H_1D, edges = np.histogram(H_sorted, bins=nbins1D)
    plt.scatter(edges[1:], 1.-np.cumsum(H_1D*1.0/(reduce(sum, H_1D))), s=size, color=color, label=label)

def make_hist(xs, ys, nbins2D, nbins1D=128, size=1.0, color='black', label=''):
    H_sorted = np.trim_zeros(np.sort(gen_hist_2d(xs, ys, nbins=nbins2D).flatten()))
    H_sorted = H_sorted*(nbins2D**2)/len(xs)
    H_1D, edges = np.histogram(H_sorted, bins=nbins1D)
    print H_1D
    plt.scatter(edges[1:], (H_1D*1.0/(reduce(sum, H_1D))), s=size, color=color, label=label)

def make_2D_hist(xs, ys, nbins=128, vmin=1.e-3, vmax=20.0, norm=False):
    H, xedges, yedges = np.histogram2d(ys, xs, bins=nbins)
    if(norm): H = H*(nbins**2)/len(xs)
    X,Y = np.meshgrid(xedges, yedges)
    plt.pcolormesh(X, Y, H, vmin=vmin, vmax=max(H.max(), vmax), norm=LogNorm())

def distance(x1, y1, x2, y2):
    return np.sqrt((x1-x2)**2 + (y1-y2)**2)

def distance_dist(xs1, ys1, xs2, ys2, N=1000, bins=100, size=1.0, color='k', label=''):
    xrand1 = np.random.choice(xs1, N)
    yrand1 = np.random.choice(ys1, N)
    xrand2 = np.random.choice(xs2, N)
    yrand2 = np.random.choice(ys2, N)
    
    distrand = distance(xrand1, yrand1, xrand2, yrand2)
    dist_hist, edges = np.histogram(distrand, bins=bins)
    plt.scatter(edges[1:], np.cumsum(dist_hist), s=size, color=color, label=label)


if __name__ == "__main__":

    xs, ys= np.loadtxt('missouri.dat', unpack=True, usecols=(0,1))
    race = np.genfromtxt('missouri.dat', usecols=2, dtype=None)

    size = 1.e-4
    nbins = 1024
    vmin=0.0
    vmax=1.e0

    xs_white, ys_white = extract_race(xs, ys, race, 'w')
    xs_black, ys_black = extract_race(xs, ys, race, 'b')
    #xs_hisp, ys_hisp = extract_race(xs, ys, race, 'h')
    #xs_asian, ys_asian = extract_race(xs, ys, race, 'a')
    #xs_other, ys_other = extract_race(xs, ys, race, 'o')


    ############################
    # Print out the total populations
    ############################

    #print len(xs)
    #print len(xs_white)
    #print len(xs_black)
    #print len(xs_hisp)
    #print len(xs_asian)
    #print len(xs_other)
    ############################
    # Scatter plots according to race
    ############################
    
    fig1=plt.figure(1)
    plt.scatter(xs_white, ys_white, s=size)
    plt.xlim(xmin=min(xs_white), xmax=max(xs_white))
    plt.ylim(ymin=min(ys_white), ymax=max(ys_white))
    plt.axis('off')
    plt.tight_layout()
    plt.savefig('white_scatter.png')
    
    fig2=plt.figure(2)
    plt.scatter(xs_black, ys_black, s=size)
    plt.xlim(xmin=min(xs_white), xmax=max(xs_white))
    plt.ylim(ymin=min(ys_white), ymax=max(ys_white))
    plt.axis('off')
    plt.tight_layout()
    plt.savefig('black_scatter.png')

    
    #fig3=plt.figure(3)
    #plt.scatter(xs_hisp, ys_hisp, s=size)
    #plt.axis('off')
    #plt.savefig('hisp_scatter.png')
    #
    #fig4=plt.figure(4)
    #plt.scatter(xs_asian, ys_asian, s=size)
    #plt.xlim(xmin = -1.07e+7, xmax=-9.8e+6)
    #plt.axis('off')
    #plt.savefig('asian_scatter.png')
    #
    #fig5=plt.figure(5)
    #plt.scatter(xs_other, ys_other, s=size)
    #plt.axis('off')
    #plt.savefig('other_scatter.png')
    #
    #############################
    ## 2D Histograms
    #############################
    #
    ## Total populations
    #for n in range(5, 11):
    #    nbins = 2**n
    #    plt.clf()
    #    make_2D_hist(xs, ys, nbins=nbins, vmin=1.e-0)
    #    plt.axis('off')
    #    plt.colorbar()
    #    filename = 'Hist2D_all_'+"%02d" % n + '.png'
    #    plt.savefig(filename)
    #
    # Normalized histograms
    #for n in range(5, 11):
    #    nbins = 2**n
    #    plt.clf()
    #    make_2D_hist(xs, ys, nbins=nbins, vmin=1.e-3, vmax=1000.0, norm=True)
    #    plt.axis('off')
    #    plt.colorbar()
    #    filename = 'Hist2D_all_norm_'+"%02d" % n + '.png'
    #    plt.savefig(filename)
    #
    ##############################
    ### Distribution of population
    ##############################

    #Total 
    #size = 1.0
    #fig7=plt.figure(7)
    #n = 5
    #for c in ['k', 'r', 'g', 'b', 'm', 'c']:
    #    print n 
    #    make_hist_sorted(xs, ys, size=size, nbins2D=2**n, color=c, label='2^'+str(n))
    #    n+=1
    #
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.xlim(xmin=1.e-4, xmax=1.0)
    #plt.ylim(ymin=1.e-3)
    #plt.legend(loc=2)
    #
    #size = 1.0
    #fig8=plt.figure(8)
    #n = 5
    #for c in ['k', 'r', 'g', 'b', 'm', 'c']:
    #    print n 
    #    make_hist_sorted(xs, ys, size=size, nbins2D=2**n, color=c, label='2^'+str(n), inv=True)
    #    n+=1
    #
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.ylim(ymin=1.e-3, ymax=1.0)
    #plt.xlim(xmin=1.e-3)
    #plt.legend(loc=3)
    #
    #size = 1.0
    #fig9=plt.figure(9)
    #n = 5
    #for c in ['k', 'r', 'g', 'b', 'm', 'c']:
    #    print n 
    #    make_comp_cumulative_dist(xs, ys, size=size, nbins2D=2**n, nbins1D=2**10, color=c, label='2^'+str(n))
    #    n+=1
    #
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.xlim(xmin=1.e-2, xmax=2.e2)
    #plt.ylim(ymin=1.e-3, ymax=1.0)
    #plt.legend(loc=3)
    #plt.xlabel(r'Population/$N^2$', fontsize=25)
    #plt.ylabel(r'P(x)', fontsize=25)
    #plt.tight_layout()
    #plt.savefig('ccdf_total_loglog.png')
    #
    #size = 1.0
    #fig10=plt.figure(10)
    #n = 5
    #for c in ['k', 'r', 'g', 'b', 'm', 'c']:
    #    print n 
    #    make_comp_cumulative_dist(xs_white, ys_white, size=size, nbins2D=2**n, nbins1D=2**10, color=c, label='2^'+str(n))
    #    n+=1
    #
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.xlim(xmin=1.e-2, xmax=1.e3)
    #plt.ylim(ymin=1.e-5, ymax=1.0)
    #plt.legend(loc=3)
    #plt.xlabel(r'Population/$N^2$', fontsize=25)
    #plt.ylabel(r'P(x)', fontsize=25)
    #plt.tight_layout()
    #plt.savefig('ccdf_white_loglog.png')
    #
    #size = 1.0
    #fig11=plt.figure(11)
    #n = 5
    #for c in ['k', 'r', 'g', 'b', 'm', 'c']:
    #    print n 
    #    make_comp_cumulative_dist(xs_black, ys_black, size=size, nbins2D=2**n, nbins1D=2**10, color=c, label='2^'+str(n))
    #    n+=1
    #
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.xlim(xmin=1.e-2, xmax=1.e3)
    #plt.ylim(ymin=1.e-5, ymax=1.0)
    #plt.legend(loc=3)
    #plt.xlabel(r'Population/$N^2$', fontsize=25)
    #plt.ylabel(r'P(x)', fontsize=25)
    #plt.tight_layout()
    #plt.savefig('ccdf_black_loglog.png')
    #
    #size = 1.0
    #fig12=plt.figure(12)
    #n = 5
    #for c in ['k', 'r', 'g', 'b', 'm', 'c']:
    #    print n 
    #    make_comp_cumulative_dist(xs_hisp, ys_hisp, size=size, nbins2D=2**n, nbins1D=2**10, color=c, label='2^'+str(n))
    #    n+=1
    #
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.xlim(xmin=1.e-2, xmax=1.e3)
    #plt.ylim(ymin=1.e-5, ymax=1.0)
    #plt.legend(loc=3)
    #plt.xlabel(r'Population/$N^2$', fontsize=25)
    #plt.ylabel(r'P(x)', fontsize=25)
    #plt.tight_layout()
    #plt.savefig('ccdf_hisp_loglog.png')
    #
    #size = 1.0
    #fig13=plt.figure(13)
    #n = 5
    #for c in ['k', 'r', 'g', 'b', 'm', 'c']:
    #    print n 
    #    make_comp_cumulative_dist(xs_asian, ys_asian, size=size, nbins2D=2**n, nbins1D=2**10, color=c, label='2^'+str(n))
    #    n+=1
    #
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.xlim(xmin=1.e-2, xmax=1.e3)
    #plt.ylim(ymin=1.e-5, ymax=1.0)
    #plt.legend(loc=3)
    #plt.xlabel(r'Population/$N^2$', fontsize=25)
    #plt.ylabel(r'P(x)', fontsize=25)
    #plt.tight_layout()
    #plt.savefig('ccdf_asian_loglog.png')
    #
    #size = 1.0
    #fig14=plt.figure(14)
    #n = 5
    #for c in ['k', 'r', 'g', 'b', 'm', 'c']:
    #    print n 
    #    make_comp_cumulative_dist(xs_other, ys_other, size=size, nbins2D=2**n, nbins1D=2**10, color=c, label='2^'+str(n))
    #    n+=1
    #
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.xlim(xmin=1.e-2, xmax=1.e3)
    #plt.ylim(ymin=1.e-5, ymax=1.0)
    #plt.legend(loc=3)
    #plt.xlabel(r'Population/$N^2$', fontsize=25)
    #plt.ylabel(r'P(x)', fontsize=25)
    #plt.tight_layout()
    #plt.savefig('ccdf_other_loglog.png')
    #
    #
    ###############################
    ### Distribution of Distance 
    ###############################
   # 
   # fig15=plt.figure(15)
   # distance_dist(xs_white, ys_white, xs_white, ys_white, N=1.e6, bins=1.e3, color='k', label='White-White')
   # distance_dist(xs_black, ys_black, xs_black, ys_black, N=1.e6, bins=1.e3, color='r', label='Black-Black')
   # distance_dist(xs_hisp, ys_hisp, xs_hisp, ys_hisp, N=1.e6, bins=1.e3, color='g', label='Hispanic-Hispanic')
   # distance_dist(xs_asian, ys_asian, xs_asian, ys_asian, N=1.e6, bins=1.e3, color='b', label='Asian-Asian')
   # distance_dist(xs_other, ys_other, xs_other, ys_other, N=1.e6, bins=1.e3, color='m', label='Other-Other')
   # plt.xscale('log')
   # plt.yscale('log')
   # plt.xlim(xmin=1.e3)
   # plt.ylim(ymin=1.0)
   # plt.legend(loc=3)
   # plt.xlabel('Distance (meters)')
   # #plt.savefig('distance_dist_same_linlin.png')
   # plt.show()

   # #fig16=plt.figure(16)
    #distance_dist(xs_white, ys_white, xs_white, ys_white, N=1.e6, bins=1.e3, color='k', label='White-White')
    #distance_dist(xs_white, ys_white, xs_black, ys_black, N=1.e6, bins=1.e3, color='r', label='White-Black')
    #distance_dist(xs_white, ys_white, xs_hisp, ys_hisp, N=1.e6, bins=1.e3, color='g', label='White-Hispanic')
    #distance_dist(xs_white, ys_white, xs_asian, ys_asian, N=1.e6, bins=1.e3, color='b', label='White-Asian')
    #distance_dist(xs_white, ys_white, xs_other, ys_other, N=1.e6, bins=1.e3, color='m', label='White-Other')
    ##plt.xscale('log')
    ##plt.yscale('log')
    #plt.xlim(xmin=1.e3)
    #plt.ylim(ymin=1.0)
    #plt.legend(loc=1)
    #plt.xlabel('Distance (meters)')
    #plt.savefig('distance_dist_white_linlin.png')

    #fig17=plt.figure(17)
    #distance_dist(xs_black, ys_black, xs_white, ys_white, N=1.e6, bins=1.e3, color='k', label='Black-White')
    #distance_dist(xs_black, ys_black, xs_black, ys_black, N=1.e6, bins=1.e3, color='r', label='Black-Black')
    #distance_dist(xs_black, ys_black, xs_hisp, ys_hisp, N=1.e6, bins=1.e3, color='g', label='Black-Hispanic')
    #distance_dist(xs_black, ys_black, xs_asian, ys_asian, N=1.e6, bins=1.e3, color='b', label='Black-Asian')
    #distance_dist(xs_black, ys_black, xs_other, ys_other, N=1.e6, bins=1.e3, color='m', label='Black-Other')
    ##plt.xscale('log')
    ##plt.yscale('log')
    #plt.xlim(xmin=1.e3)
    #plt.ylim(ymin=1.0)
    #plt.legend(loc=1)
    #plt.xlabel('Distance (meters)')
    #plt.savefig('distance_dist_black_linlin.png')

    #fig18=plt.figure(18)
    #distance_dist(xs_hisp, ys_hisp, xs_white, ys_white, N=1.e6, bins=1.e3, color='k', label='Hispanic-White')
    #distance_dist(xs_hisp, ys_hisp, xs_black, ys_black, N=1.e6, bins=1.e3, color='r', label='Hispanic-Black')
    #distance_dist(xs_hisp, ys_hisp, xs_hisp, ys_hisp, N=1.e6, bins=1.e3, color='g', label='Hispanic-Hispanic')
    #distance_dist(xs_hisp, ys_hisp, xs_asian, ys_asian, N=1.e6, bins=1.e3, color='b', label='Hispanic-Asian')
    #distance_dist(xs_hisp, ys_hisp, xs_other, ys_other, N=1.e6, bins=1.e3, color='m', label='Hispanic-Other')
    ##plt.xscale('log')
    ##plt.yscale('log')
    #plt.xlim(xmin=1.e3)
    #plt.ylim(ymin=1.0)
    #plt.legend(loc=1)
    #plt.xlabel('Distance (meters)')
    #plt.savefig('distance_dist_hisp_linlin.png')

    #fig19=plt.figure(19)
    #distance_dist(xs_asian, ys_asian, xs_white, ys_white, N=1.e6, bins=1.e3, color='k', label='Asian-White')
    #distance_dist(xs_asian, ys_asian, xs_black, ys_black, N=1.e6, bins=1.e3, color='r', label='Asian-Black')
    #distance_dist(xs_asian, ys_asian, xs_hisp, ys_hisp, N=1.e6, bins=1.e3, color='g', label='Asian-Hispanic')
    #distance_dist(xs_asian, ys_asian, xs_asian, ys_asian, N=1.e6, bins=1.e3, color='b', label='Asian-Asian')
    #distance_dist(xs_asian, ys_asian, xs_other, ys_other, N=1.e6, bins=1.e3, color='m', label='Asian-Other')
    ##plt.xscale('log')
    ##plt.yscale('log')
    #plt.xlim(xmin=1.e3)
    #plt.ylim(ymin=1.0)
    #plt.legend(loc=1)
    #plt.xlabel('Distance (meters)')
    #plt.savefig('distance_dist_asian_linlin.png')

    #fig20=plt.figure(20)
    #distance_dist(xs_other, ys_other, xs_white, ys_white, N=1.e6, bins=1.e3, color='k', label='Other-White')
    #distance_dist(xs_other, ys_other, xs_black, ys_black, N=1.e6, bins=1.e3, color='r', label='Other-Black')
    #distance_dist(xs_other, ys_other, xs_hisp, ys_hisp, N=1.e6, bins=1.e3, color='g', label='Other-Hispanic')
    #distance_dist(xs_other, ys_other, xs_asian, ys_asian, N=1.e6, bins=1.e3, color='b', label='Other-Asian')
    #distance_dist(xs_other, ys_other, xs_other, ys_other, N=1.e6, bins=1.e3, color='m', label='Other-Other')
    ##plt.xscale('log')
    ##plt.yscale('log')
    #plt.xlim(xmin=1.e3)
    #plt.ylim(ymin=1.0)
    #plt.legend(loc=1)
    #plt.xlabel('Distance (meters)')
    #plt.savefig('distance_dist_other_linlin.png')


