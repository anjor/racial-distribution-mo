###########################################################################
# Makefile to generate report pdf
#			by Anjor Kanekar	Last modified: 01/9/15
###########################################################################

#Put everything that needs to be put into tar file into pack, including 
#  all necessary subdirectories
PACK =	Makefile MO_report.tex *png
SH =	`which sh`
CSH =	`which csh`

###########################################################################
#

all:	report

usage:	
	echo "make usage				- shows this text"; \
	echo "make report					- runs report through LaTeX, without bib"; \
	echo "make bib					- sets up bibliography entries"; \
	echo "make pack					- makes an archive of the documentation"

report:	MO_report.tex 
	${SH} -c "pdflatex MO_report.tex"

clean: 
	rm -f *aux

completeclean: 
	rm -f *aux
	rm -f *bbl *blg *log *lof *toc


#bib:	
#	echo "Attempting to make the bibliography.....";\
#	${SH} -c "bibtex MO_report; pdflatex MO_report; pdflatex MO_report"

pack:		
	tar cvf report`date +'%y%m%d'`.tar ${PACK} ; gzip report`date +'%y%m%d'`.tar
